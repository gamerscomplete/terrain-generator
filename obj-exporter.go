package terrainGenerator

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const (
	MAT_NAME = "Suzmat"
)

func (patch *Patch) ExportObj(file string) error {
	directory := filepath.Dir(file)
	if _, err := os.Stat(directory); os.IsNotExist(err) {
		os.MkdirAll(directory, os.ModePerm)
	}

	f, err := os.Create(file)
	if err != nil {
		return err
	}
	defer f.Close()

	pathSplit := strings.Split(filepath.Base(file), ".")
	mtlFileName := directory + "/" + pathSplit[0] + ".mtl"

	mtlFile, err := os.Create(mtlFileName)
	if err != nil {
		return err
	}
	defer mtlFile.Close()

	//TODO: Make this non static
	name := "terrain patch"

	var buffer bytes.Buffer

	//o - name
	buffer.WriteString("o " + name + "\n")
	//g - group
	buffer.WriteString("g Patch\n")

	for i := 0; i < len(patch.Vertices); i++ {
		buffer.WriteString("v " + stringify(-patch.Vertices[i].X) + " " + stringify(patch.Vertices[i].Y) + " " + stringify(patch.Vertices[i].Z) + "\n")
		//		buffer.WriteString("v " + stringify(patch.Vertices[i].X) + " " + stringify(patch.Vertices[i].Y) + " " + stringify(patch.Vertices[i].Z) + "\n")
	}

	for i := 0; i < len(patch.Normals); i++ {
		buffer.WriteString("vn " + stringify(-patch.Normals[i].X) + " " + stringify(patch.Normals[i].Y) + " " + stringify(patch.Normals[i].Z) + "\n")
		//		buffer.WriteString("vn " + stringify(patch.Normals[i].X) + " " + stringify(patch.Normals[i].Y) + " " + stringify(patch.Normals[i].Z) + "\n")
	}

	for i := 0; i < len(patch.UV); i++ {
		buffer.WriteString("vt " + stringify(patch.UV[i].X) + " " + stringify(patch.UV[i].Y) + "\n")
	}

	buffer.WriteString("usemtl " + MAT_NAME + "\n")
	buffer.WriteString("s 1\n\n")

	for i := 0; i < len(patch.Triangles); i += 3 {
		buffer.WriteString("f " + fmt.Sprint(patch.Triangles[i]+1) + "/" + fmt.Sprint(patch.Triangles[i]+1) + "/" + fmt.Sprint(patch.Triangles[i]+1))
		buffer.WriteString(" " + fmt.Sprint(patch.Triangles[i+2]+1) + "/" + fmt.Sprint(patch.Triangles[i+2]+1) + "/" + fmt.Sprint(patch.Triangles[i+2]+1))
		buffer.WriteString(" " + fmt.Sprint(patch.Triangles[i+1]+1) + "/" + fmt.Sprint(patch.Triangles[i+1]+1) + "/" + fmt.Sprint(patch.Triangles[i+1]+1) + "\n")
	}

	//	written, err := f.Write(buffer.Bytes())
	_, err = f.Write(buffer.Bytes())
	if err != nil {
		return err
	}

	//	fmt.Println("Wrote patch to:", file, " size:", written)

	//Write .mtl file out
	if _, err := mtlFile.WriteString(basicMaterial); err != nil {
		return err
	}
	mtlFile.Sync()
	return nil
}

func stringify(input float32) string {
	//	return string(int(input))
	return strconv.FormatFloat(float64(input), 'f', 2, 32)
}
