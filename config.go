package terrainGenerator

import (
	"runtime"
)

var DefaultConfig = &Config{
	ThreadCount: runtime.NumCPU(),
	OutputDir:   "assets",
	//Make sure odd number for PatchVertexCount
	VertexCount:   51,
	VertexSpacing: 2,
	MaxHeight:     128,
	//	MaxHeight:           2048,
	WorldDataTex:        "assets/worldData.png",
	SmoothingIterations: 12,
	SmoothingAmount:     0.35,
	WorldSize:           81920,
	MapSize:             2048,
	//larger the bias value the narrower the beaches
	BeachBias: 5,

	//Noise scaling
	LowFreq:   0.1,
	MedFreq:   0.25,
	HighFreq:  1,
	VHighFreq: 4,

	//mapsize / worldsize
	SampleFreq: 0.025,

	Blend1: 0.5,
	Blend2: 0.55,
	Blend3: 0.9,
	Blend4: 0.4,
}

type Config struct {
	//General config
	OutputDir string

	ThreadCount         int
	VertexSpacing       int
	VertexCount         int
	MaxHeight           float32
	WorldDataTex        string
	SmoothingIterations int
	SmoothingAmount     float32
	WorldSize           int
	MapSize             int
	BeachBias           float32

	//Noise scaling
	LowFreq    float32
	MedFreq    float32
	HighFreq   float32
	VHighFreq  float32
	SampleFreq float32
	Blend1     float32
	Blend2     float32
	Blend3     float32
	Blend4     float32
}

func (config *Config) PatchSize() int {
	return (config.VertexCount - 1) * config.VertexSpacing
}
