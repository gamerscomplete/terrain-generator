package terrainGenerator

import (
	//	"gitlab.com/g3n/engine/geometry"
	"fmt"
	"gitlab.com/g3n/engine/math32"
	"image"
	"math"
)

type TerrainGenerator struct {
	Config
	basePatch *Patch
	imageData image.Image

	imageMap [4][][]uint8
}

func NewTerrainGenerator(config *Config) (*TerrainGenerator, error) {
	tgen := TerrainGenerator{}
	tgen.Config = *config
	tgen.generateBasePatch()

	//TODO: Load image, return error if fail
	if err := tgen.loadImage(); err != nil {
		return &tgen, err
	}
	return &tgen, nil
}

func (tgen *TerrainGenerator) GeneratePatch(position math32.Vector2) *Patch {
	patch := tgen.basePatch.Copy()

	//Load height
	for i := 0; i < len(patch.Vertices); i++ {
		patch.Vertices[i].Y = tgen.getSmoothHeight(
			patch.Vertices[i].X+float32(int(position.X)*((tgen.VertexCount-1)*tgen.VertexSpacing)),
			patch.Vertices[i].Z+float32(int(position.Y)*((tgen.VertexCount-1)*tgen.VertexSpacing)),
		)
	}
	return patch
}

func (tgen *TerrainGenerator) generateBasePatch() {
	patch := NewPatch()
	verticeCount := tgen.VertexCount * tgen.VertexCount

	patch.createVertices(tgen.VertexCount, tgen.VertexSpacing)
	patch.createTriangles(tgen.VertexCount)
	patch.createNormals(verticeCount)
	patch.createUVs(verticeCount, tgen.VertexCount, tgen.VertexSpacing)

	tgen.basePatch = patch
}

func (tgen *TerrainGenerator) getSmoothHeight(x, z float32) float32 {
	height := tgen.getHeight(x, z)
	//	fmt.Println("initial height:", height)
	hs := float32(tgen.VertexSpacing / 2)
	ul := tgen.getHeight(x-hs, z+float32(tgen.VertexSpacing))
	ur := tgen.getHeight(x+hs, z+float32(tgen.VertexSpacing))
	lt := tgen.getHeight(x-float32(tgen.VertexSpacing), z)
	rt := tgen.getHeight(x+float32(tgen.VertexSpacing), z)
	ll := tgen.getHeight(x-hs, z-float32(tgen.VertexSpacing))
	lr := tgen.getHeight(x+hs, z-float32(tgen.VertexSpacing))

	avgHeight := (ul + ur + lt + rt + ll + lr) / 6

	adaptiveIterations := int(math.Abs(float64(height - avgHeight)))

	if adaptiveIterations > 10 {
		adaptiveIterations = 10
	}

	for i := 0; i < tgen.SmoothingIterations+adaptiveIterations; i++ {
		height = lerp(height, avgHeight, tgen.SmoothingAmount)
	}

	//	fmt.Println("Setting height:", height)
	if height > tgen.MaxHeight {
		fmt.Println("Bottomed out:", height)
		return tgen.MaxHeight
	}

	return height
}

func (tgen *TerrainGenerator) getHeight(x, z float32) float32 {
	//	fmt.Println("Getting height for:", x, z)
	height1 := tgen.sampleMap(x, z, tgen.LowFreq, 1)
	height2 := tgen.sampleMap(x, z, tgen.MedFreq, 2)
	height3 := tgen.sampleMap(x, z, tgen.HighFreq, 3)
	height4 := tgen.sampleMap(x, z, tgen.VHighFreq, 3)
	height5 := tgen.sampleMap(x, z, tgen.SampleFreq, 0)

	//	fmt.Println("Height dump:", height1, height2, height3, height4, height5)

	blend1 := height1*tgen.Blend1 + height2*(1-tgen.Blend1)
	blend2 := blend1*tgen.Blend2 + height3*(1-tgen.Blend2)
	blend3 := blend2*tgen.Blend3 + height4*(1-tgen.Blend2)
	blend4 := height5*tgen.Blend4 + blend3*(1-tgen.Blend4)

	//	fmt.Println("Blend dump:", blend1, blend2, blend3, blend4)

	flattenHeight := float32(math.Pow(float64(blend4*2-1), 3))
	//	fmt.Println("flatten:", flattenHeight)

	//Makes anything below half a negative number. keeps values between -1 and 1
	//compensates for sea level
	blend4 = (blend4 - 0.5) * 2
	if blend4 < 0 {
		//		fmt.Println("Dumping blend4:", blend4)
		blend4 /= 2
	}

	//	fmt.Println("Preheight dump:", flattenHeight, blend4, float32(math.Abs(float64(blend4))), tgen.BeachBias)
	//larger the bias value the narrower the beaches
	height := lerp(flattenHeight, blend4, float32(math.Abs(float64(blend4)))*tgen.BeachBias) * tgen.MaxHeight
	//	fmt.Println("Finished height:", height)

	return height
}

func (tgen *TerrainGenerator) sampleMap(x, z, scale float32, layer int) float32 {
	halfMapSize := float32(tgen.MapSize / 2)
	scaledX := x*scale + halfMapSize
	scaledZ := z*scale + halfMapSize

	fX := int(math.Floor(float64(scaledX))) % tgen.MapSize
	if fX < 0 {
		fX = tgen.MapSize + fX
	}

	fZ := int(math.Floor(float64(scaledZ))) % tgen.MapSize
	if fZ < 0 {
		fZ = tgen.MapSize + fZ
	}

	cX := int(math.Ceil(float64(scaledX))) % tgen.MapSize
	if cX < 0 {
		cX = tgen.MapSize + cX
	}

	cZ := int(math.Ceil(float64(scaledZ))) % tgen.MapSize
	if cZ < 0 {
		cZ = tgen.MapSize + cZ
	}

	rX := scaledX - float32(int(scaledX))
	rZ := scaledZ - float32(int(scaledZ))

	height1 := tgen.imageMap[layer][fX][fZ]
	height2 := tgen.imageMap[layer][cX][fZ]
	height3 := tgen.imageMap[layer][fX][cZ]
	height4 := tgen.imageMap[layer][cX][cZ]

	//	fmt.Println("Preblender:", height1, height2, height3, height4, rX)

	//	blend1 := lerp(float32(height1/255), float32(height2/255), rX)
	//	blend2 := lerp(float32(height3/255), float32(height4/255), rX)

	blend1 := lerp(float32(height1)/255, float32(height2)/255, rX)
	blend2 := lerp(float32(height3)/255, float32(height4)/255, rX)

	//	blend1 := lerp(float32(height1), float32(height2), rX)
	//	blend2 := lerp(float32(height3), float32(height4), rX)

	//	fmt.Println("Blend dump:", blend1, blend2)
	//	fmt.Println("Shit:", blend1, blend2, rZ)
	height := lerp(blend1, blend2, rZ)
	//	fmt.Println("Heighted:", height)
	return height
}

func lerp(v1, v2, amt float32) float32 {
	val := v1 + ((v2 - v1) * amt)
	return clamp(v1, v2, val)
}

func clamp(min, max, val float32) float32 {
	if max < min {
		min, max = max, min
	}
	if val < min {
		val = min
	} else if val > max {
		val = max
	}
	return val
}
