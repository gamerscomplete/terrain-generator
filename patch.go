package terrainGenerator

import (
	"fmt"
	"gitlab.com/g3n/engine/geometry"
	"gitlab.com/g3n/engine/gls"
	"gitlab.com/g3n/engine/math32"
	"math"
)

type Patch struct {
	Vertices  []math32.Vector3
	Triangles math32.ArrayU32
	Normals   []math32.Vector3
	UV        []math32.Vector2
	geometry.Geometry
}

func NewPatch() *Patch {
	return new(Patch)
}

func (patch *Patch) Copy() *Patch {
	newPatch := NewPatch()
	newPatch.Vertices = append([]math32.Vector3(nil), patch.Vertices...)
	newPatch.Triangles = append(math32.ArrayU32(nil), patch.Triangles...)
	newPatch.Normals = append([]math32.Vector3(nil), patch.Normals...)
	newPatch.UV = append([]math32.Vector2(nil), patch.UV...)
	return newPatch
}

func (patch *Patch) CreateGeometry() {
	if patch == nil {
		fmt.Println("wtf")
	}
	geom := *geometry.NewGeometry()

	patch.Geometry = geom

	patch.SetIndices(patch.Triangles)

	var verts = math32.NewArrayF32(0, len(patch.Vertices)*3)

	//This is ugly and slow but lets at least get it working
	for _, value := range patch.Vertices {
		verts.AppendVector3(&value)
	}
	//	verts.AppendVector3(patch.Vertices...)
	//	patch.AddVBO(gls.NewVBO().AddAttrib("VertexPosition", 3).SetBuffer(verts))
	patch.AddVBO(gls.NewVBO(verts).AddAttrib(gls.VertexPosition))

	var normals = math32.NewArrayF32(0, len(patch.Normals)*3)
	for _, value := range patch.Normals {
		normals.AppendVector3(&value)
	}

	//	patch.AddVBO(gls.NewVBO().AddAttrib("VertexNormal", 3).SetBuffer(normals))
	patch.AddVBO(gls.NewVBO(normals).AddAttrib(gls.VertexNormal))

	var uv = math32.NewArrayF32(0, len(patch.UV)*2)

	for _, value := range patch.UV {
		uv.AppendVector2(&value)
	}
	//	patch.AddVBO(gls.NewVBO().AddAttrib("VertexTexcoord", 2).SetBuffer(uv))
	patch.AddVBO(gls.NewVBO(uv).AddAttrib(gls.VertexTexcoord))
}

func (patch *Patch) createVertices(vertexCount int, vertexSpacing int) {
	var offset, posX, posZ int
	offset = (vertexCount - 1) / 2 * vertexSpacing
	patch.Vertices = make([]math32.Vector3, vertexCount*vertexCount)
	for z := 0; z < vertexCount; z++ {
		for x := 0; x < vertexCount; x++ {
			posX = x*vertexSpacing - offset
			posZ = z*vertexSpacing - offset
			patch.Vertices[(z*vertexCount)+x] = math32.Vector3{X: float32(posX), Y: 0, Z: float32(posZ)}
		}
	}
}

func (patch *Patch) createUVs(count int, vertexCount int, vertexSpacing int) {
	offset := (vertexCount - 1) / 2 * vertexSpacing

	patch.UV = make([]math32.Vector2, count)
	for i := 0; i < count; i++ {
		patch.UV[i] = math32.Vector2{
			X: 1 - (patch.Vertices[i].X+float32(offset))/float32(((vertexCount-1)*vertexSpacing)),
			Y: 1 - (patch.Vertices[i].Z+float32(offset))/float32(((vertexCount-1)*vertexSpacing)),
		}
	}
}

func (patch *Patch) createNormals(count int) {
	patch.Normals = make([]math32.Vector3, count)
	for i := 0; i < count; i++ {
		patch.Normals[i] = math32.Vector3{-1, 1, 1}
	}
}

func (patch *Patch) createTriangles(vertexCountInput int) {
	vertexCount := uint32(vertexCountInput)
	step := uint32(2)
	offset := step / 2
	var index uint32
	var offsetSize uint32
	patch.Triangles = make([]uint32, int(math.Pow(float64((vertexCount-offset)/step), 2)*24))

	var iterator int
	for z := offset; z < vertexCount; z += step {
		for x := offset; x < vertexCount; x += step {
			index = z*vertexCount + x
			offsetSize = vertexCount * offset

			//1
			patch.Triangles[iterator*24] = index + offsetSize - offset
			patch.Triangles[iterator*24+1] = index + offsetSize
			patch.Triangles[iterator*24+2] = index

			//2
			patch.Triangles[iterator*24+3] = index + offsetSize
			patch.Triangles[iterator*24+4] = index + offsetSize + offset
			patch.Triangles[iterator*24+5] = index

			//3
			patch.Triangles[iterator*24+6] = index + offsetSize + offset
			patch.Triangles[iterator*24+7] = index + offset
			patch.Triangles[iterator*24+8] = index

			//4
			patch.Triangles[iterator*24+9] = index + offset
			patch.Triangles[iterator*24+10] = index - offsetSize + offset
			patch.Triangles[iterator*24+11] = index

			//5
			patch.Triangles[iterator*24+12] = index - offsetSize + offset
			patch.Triangles[iterator*24+13] = index - offsetSize
			patch.Triangles[iterator*24+14] = index

			//6
			patch.Triangles[iterator*24+15] = index - offsetSize
			patch.Triangles[iterator*24+16] = index - offsetSize - offset
			patch.Triangles[iterator*24+17] = index

			//7
			patch.Triangles[iterator*24+18] = index - offsetSize - offset
			patch.Triangles[iterator*24+19] = index - offset
			patch.Triangles[iterator*24+20] = index

			//8
			patch.Triangles[iterator*24+21] = index - offset
			patch.Triangles[iterator*24+22] = index + offsetSize - offset
			patch.Triangles[iterator*24+23] = index

			iterator++
		}
	}
}
