package terrainGenerator

import (
	"fmt"
	"image"
	_ "image/png"
	"os"
)

func (tgen *TerrainGenerator) loadImage() error {
	infile, err := os.Open(tgen.WorldDataTex)
	if err != nil {
		return err
	}
	defer infile.Close()

	img, _, err := image.Decode(infile)
	if err != nil {
		return err
	}
	bounds := img.Bounds()
	width, height := bounds.Max.X, bounds.Max.Y

	//	tgen.imageMap = make([4][][]uint8)
	tgen.imageMap[0] = make([][]uint8, width)
	tgen.imageMap[1] = make([][]uint8, width)
	tgen.imageMap[2] = make([][]uint8, width)
	tgen.imageMap[3] = make([][]uint8, width)

	for x := 0; x < width; x++ {
		tgen.imageMap[0][x] = make([]uint8, height)
		tgen.imageMap[1][x] = make([]uint8, height)
		tgen.imageMap[2][x] = make([]uint8, height)
		tgen.imageMap[3][x] = make([]uint8, height)
		for y := 0; y < height; y++ {
			r, g, b, a := img.At(x, y).RGBA()
			if (r/257) > 255 || (g/257) > 255 || (b/257) > 255 || (a/257) > 255 {
				fmt.Println("boom:", r, g, b, a)
			}
			//			fmt.Println("R:", r, "G:", g, "B:", b, "A:", a)
			/*
				tgen.imageMap[0][x][y] = uint8(r / 257)
				tgen.imageMap[1][x][y] = uint8(g / 257)
				tgen.imageMap[2][x][y] = uint8(b / 257)
				tgen.imageMap[3][x][y] = uint8(a / 257)
			*/
			tgen.imageMap[0][x][y] = uint8(a / 257)
			tgen.imageMap[1][x][y] = uint8(r / 257)
			tgen.imageMap[2][x][y] = uint8(g / 257)
			tgen.imageMap[3][x][y] = uint8(b / 257)
		}
	}
	//	fmt.Println("Shit:", tgen.imageMap)
	return nil
}
