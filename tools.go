package terrainGenerator

import (
	"gitlab.com/g3n/engine/math32"
)

const (
	//This is fucky. Dont even remember why im doing it like this
	ZONE_SIZE = 10
)

func (tgen *TerrainGenerator) GetZone(position math32.Vector2) math32.Vector2 {
	var x = int(position.X / ZONE_SIZE)
	var y = int(position.Y / ZONE_SIZE)

	if x == 0 {
		if position.X >= 0 {
			x = 1
		} else {
			x = -1
		}
	} else {
		x += sign(x)
	}

	if y == 0 {
		if position.Y >= 0 {
			y = 1
		} else {
			y = -1
		}
	} else {
		y += sign(y)
	}
	return math32.Vector2{X: float32(x), Y: float32(y)}
}

/*
func (tgen *TerrainGenerator) GetPatch(position math32.Vector3) math32.Vector2 {
    return math32.Vector2{X: float32(int(position.X) / PATCH_SIZE), Y: float32(int(position.Z) / PATCH_SIZE)}
}
*/

func sign(input int) int {
	if input >= 0 {
		return 1
	}
	return -1
}
